<?php
/*
Plugin Name: Product Data
Plugin URI: http://www.logicsbuffer.com
Description: Plugin for product data [product-data]
Version: 1.0.0
Author: LogicsBuffer
Author URI: http://www.logicsbuffer.com
Text Domain: product-data
Domain Path: languages
Author URI: http://www.logicsbuffer.com
License: GPLv2
Copyright 2020 LogicsBuffer 
*/

function prod_data_func() {
	global $product;
	$prod_id = $product->get_id();
	
	//echo $prod_id;
	//$video_url = get_field( "upload_video", $prod_id );
	//echo $video_url;
	$file = get_field("upload_video", $prod_id);
	//print_r($atts['desc']);


	if( $file ){
	    		
		$video_url = $file['url'];
		//print_r($video_url);
	    ob_start();
	    ?>
	    <video width="300" height="180" controls>
		  <source src="<?php echo esc_html($video_url); ?>" type="video/mp4">
		  <source src="<?php echo esc_html($video_url); ?>" type="video/ogg">
		Your browser does not support the video tag.
		</video>
		<?php 	
	}

	$output_string = ob_get_contents();
	ob_end_clean();
	return $output_string;

}
add_shortcode( 'product-data', 'prod_data_func' );


function prod_desc_func() {
	//Description
	global $product;
	$output_desc = $product->get_description();    
	
	return $output_desc;

}
add_shortcode( 'product-desc', 'prod_desc_func' );

function prod_payment_func() {
	//Description
	global $product;
	//add_action( 'woocommerce_checkout_order_review', 'woocommerce_order_review', 10 );
	//add_action( 'woocommerce_checkout_order_review', 'woocommerce_checkout_payment', 20 );

}
add_shortcode( 'product-payment', 'prod_payment_func' );

function prod_tags_func() {
	//Description
	global $product;
	$prod_id = $product->get_id();
	$terms = get_the_terms( $prod_id, 'product_tag' );
	ob_start();

	//print_r($terms); 
	//$terms = get_terms(array('taxonomy' => 'product_tag', 'hide_empty' => false));
	?><div class="product-tags">
	<?php foreach ( $terms as $term ) { ?>
	    <a href="<?php echo get_term_link( $term->term_id, 'product_tag' ); ?> " rel="tag"><?php echo $term->name; ?></a>
	<?php } ?>
	</div><?php

	$output_string = ob_get_contents();
	ob_end_clean();
	return $output_string;	
}
add_shortcode( 'product-tags', 'prod_tags_func' );


function prod_ext_data_func() {
	//Description
	global $product;
	$prod_id = $product->get_id();
	
	$prod_sku = $product->get_sku();
	$design_code = get_field("design_code", $prod_id);
	$color = get_field("color", $prod_id);
	$fabric = get_field("fabric", $prod_id);
	$work_details = get_field("work_details", $prod_id);
	
	?><div class="prod_ext_data_main"><?php 
	if( $prod_sku ){ ?>
	    <div class="first_chunk data_inner prod_sku"><div class="title">SKU:</div> <div class="data"><?php echo $prod_sku; ?></div></div>	
	<?php }
	if( $design_code ){ ?>
	    <div class="first_chunk first_div data_inner design_code"><div class="title">Design Code:</div> <div class="data"><?php echo $design_code; ?></div></div>		
	<?php }
	if( $color ){ ?>
	    <div class="second_chunk data_inner color"><div class="title">Color:</div> <div class="data"><?php echo $color; ?></div></div>		
	<?php }
	if( $fabric ){ ?>
	    <div class="second_chunk data_inner fabric"><div class="title">Fabric:</div> <div class="data"><?php echo $fabric; ?></div></div>
	<?php }
	if( $work_details ){ ?>
	    <div class="second_chunk data_inner work_details"><div class="title">Work Details:</div> <div class="data"><?php echo $work_details; ?></div></div>		
	<?php }
	?></div> <?php 


}
add_shortcode( 'product-ext-data', 'prod_ext_data_func' );

// To change add to cart text on single product page
add_filter( 'woocommerce_product_single_add_to_cart_text', 'woocommerce_custom_single_add_to_cart_text' ); 
function woocommerce_custom_single_add_to_cart_text() {
    return __( 'Buy Now', 'woocommerce' ); 
}

// define the woocommerce_after_single_product_summary callback 
function action_woocommerce_after_single_product_summary( $evolve_woocommerce_after_single_product_summary, $int ) { 
    echo do_shortcode('[woo-related]');  
}; 
         
// add the action 
add_action( 'woocommerce_after_single_product_summary', 'action_woocommerce_after_single_product_summary', 10, 2 ); 


//After related Products
function woocommerce_output_related_products_after() { 

    echo '<div class="row row-collapse row-full-width align-center" id="row-1655648279">
	<div class="col medium-4 small-12 large-4"><div class="col-inner text-center" style="background-color:rgb(228, 228, 228);">
	<div class="before_ftr_btns">
	<h2 class="uppercase">Unstitched</h2>
	<h3 class="thin-font">Shipping World Wide</h3>
	</div>
	</div></div>
	<div class="col medium-4 small-12 large-4"><div class="col-inner text-center" style="background-color:rgb(228, 228, 228);">
	<div class="before_ftr_btns middle">
	<h2 class="uppercase">READY TO WEAR</h2>
	<h3 class="thin-font">14 Days of Exchange</h3>
	</div>
	</div></div>
	<div class="col medium-4 small-12 large-4"><div class="col-inner text-center" style="background-color:rgb(228, 228, 228);">
	<div class="before_ftr_btns">
	<h2 class="uppercase">FREDOM TO BUY</h2>
	<h3 class="thin-font">Safe Payments</h3>
	</div>
	</div></div>
	</div>';
}; 
         
// add the action 
add_action( 'woocommerce_after_single_product', 'woocommerce_output_related_products_after', 20 );

